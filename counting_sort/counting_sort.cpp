#include "stdafx.h"
#include <iostream>

void print(int* arr, int size)
{
	for (int i = 0; i < size; i++)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}

int max(int* arr, int size)
{
	int max = arr[0];
	for (int i = 1; i < size; i++)
	{
		if (arr[i] > max)
			max = arr[i];
	}
	
	return max;
}

int* counting(int* arr, int size)
{
	int counter_size = max(arr, size) + 1;
	int* counter = new int[counter_size] {0};
	int* output = new int[size];

	for (int i = 0; i < size; i++)
		counter[arr[i]]++;

	for (int i = 1; i < counter_size; i++)
		counter[i] += counter[i - 1];

	//Сеттим аутпут
	int last = 0;
	for (int i = 0; i < counter_size; i++)
	{
		while (last < counter[i] && i < counter_size)
			output[last++] = i;
	}
	
	return output;
}

int main()
{
	int size, *arr;

	std::cout << "Enter size: ";
	std::cin >> size;

	arr = new int[size];
	int value;
	for (int i = 0; i < size; i++)
	{
		std::cout << "Enter " << i + 1 << " element: ";
		std::cin >> value;
		arr[i] = value;
	}

	int* sorted = counting(arr, size);

	print(sorted, size);

    return 0;
}