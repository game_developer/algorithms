#include "stdafx.h"
#include <iostream>

void print(int arr[], int size)
{
	for (int i = 0; i < size; i++)
		printf("%d ", arr[i]);

	std::cout << "\n";
}

void swap(int arr[], int i, int j)
{
	int temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
}

void vstavki(int arr[], int size)
{
	for (int i = 1; i < size; i++)
	{
		//сохраняем
		int value = arr[i];
		int j = i;

		while (j > 0 && arr[j - 1] > value)
		{
			arr[j] = arr[j - 1];
			j--;
		}

		arr[j] = value;



		print(arr, size);
	}
}

void vibora(int arr[], int size)
{
	for (int i = 0; i < size - 1; i++)
	{
		int min = i;

		for (int j = i + 1; j < size; j++)
		{
			if (arr[j] < arr[min])
				min = j;    // апдейтим индекс минимального елемента
		}

		swap(arr, min, i);

		print(arr, size);
	}
}

void bulbashka(int arr[], int size)
{
	for (int k = 0; k < size - 1; k++)
	{
		for (int i = k + 1; i < size; i++)
		{
			//меняем
			if (arr[k] > arr[i])
				swap(arr, k, i);
		}
	}
}

void shaker(int arr[], int size)
{
	int i, j, k;
	for (i = 0; i < size; i++)
	{
		//туда
		for (j = i + 1; j < size; j++)
		{
			if (arr[j] < arr[j - 1])
			{
				swap(arr, j, j - 1);
				print(arr, size);
			}
		}
		size--;

		printf("-----------------\n");

		//сюда
		for (k = size - 1; k > i; k--)
		{
			if (arr[k] < arr[k - 1])
			{
				swap(arr, k, k - 1);
				print(arr, size);
			}
		}
	}
}

void shell(int arr[], int size)
{
	for (int promezhytok = size / 2; promezhytok > 0; promezhytok /= 2)
	{
		for (int i = promezhytok; i < size; i += 1)
		{
			int val = arr[i];

			int j;
			for (j = i; j >= promezhytok && arr[j - promezhytok] > val; j -= promezhytok)
				arr[j] = arr[j - promezhytok];

			arr[j] = val;

			print(arr, size);
		}

		printf("\n");
	}
}

int main()
{
	int arr[] = { 3, 1, 6, 5,9,12,24,1,10,3 };
	int n = sizeof(arr) / sizeof(arr[0]);

	shaker(arr, n);

	print(arr, n);

	return 0;
}