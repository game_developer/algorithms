#pragma once
#define SIZE 5
#include <string>

using namespace std;

class queue
{
private:
	string data[SIZE];
	int last;
public:
	queue();
	~queue();
	void print();
	void enqueue(std::string el);
	string dequeue();
	bool is_full();
	bool is_empty();
};

