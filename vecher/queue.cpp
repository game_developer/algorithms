#include "stdafx.h"
#include "queue.h"
#include <iostream>

using namespace std;

queue::queue()
{
	last = -1;
}


queue::~queue()
{
}

void queue::print()
{
	for(int i = 0; i <= last; i++)
	{
		cout << data[i] << endl;
	}
}

void queue::enqueue(string el)
{
	if (is_full())
		return;

	data[++last] = el;
}

string queue::dequeue()
{
	if (is_empty())
		return nullptr;

	string del = data[0];

	for(int i = 0; i < last; i++)
	{
		data[i] = data[i + 1];
	}

	data[last--].clear();
	
	return del;
}

bool queue::is_full()
{
	return last + 1 == SIZE;
}

bool queue::is_empty()
{
	return last == -1;
}
