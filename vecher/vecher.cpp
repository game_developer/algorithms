#include "stdafx.h"
#include "queue.h"
#include <iostream>

using namespace std;

int main()
{
	queue q;

	q.enqueue("1");
	q.enqueue("2");
	q.enqueue("3");

	q.print();

	cout << endl;

	q.dequeue();
	q.dequeue();
	q.dequeue();

	q.enqueue("1");
	q.enqueue("2");
	q.enqueue("3");
	q.enqueue("4");
	q.enqueue("5");

	q.dequeue();
	q.dequeue();
	q.dequeue();
	q.dequeue();
	q.dequeue();

	

	q.enqueue("1");
	q.enqueue("2");
	q.enqueue("3");
	q.enqueue("4");
	q.enqueue("5");
	q.print();


	cout << q.is_full() << endl;

    return 0;
}

