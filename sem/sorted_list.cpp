#include "stdafx.h"
#include <iostream>

struct node
{
	int data;
	node* next;

	node(int data)
	{
		this->data = data;
		next = nullptr;
	}
};

class sorted_list
{
	node* head = nullptr;
public:
	int length = 0;

	int get_element_at(int index)
	{
		if (length - 1 < index || index < 0)
			return NULL;
		
		node* helper = head;
		for (int i = 1; i <= index; i++)
		{
			helper = helper->next;
		}
		return helper->data;
	}

	void add(int data)
	{
		node* add = new node(data);

		if (head == nullptr)
			head = add;

		else
		{
			node* temp;
			
			if (data < head->data)
			{
				temp = head;
				head = add;
				head->next = temp;
			}
			
			else
			{
				node* previous = head;
				
				while (previous->next != nullptr && data > previous->next->data)
					previous = previous->next;

				temp = previous->next;
				previous->next = add;
				add->next = temp;
			}
		}
		++length;
	}

	void print()
	{
		node* helper = head;

		while (helper != nullptr)
		{
			std::cout << helper->data << " ";
			helper = helper->next;
		}

		std::cout << "\n";
	}
};