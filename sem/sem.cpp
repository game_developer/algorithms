#include "stdafx.h"
#include <iostream>
#include "sorted_list.cpp"

sorted_list s[10];

void print(int* arr, int size)
{
	for (int i = 0; i < size; i++)
		std::cout << arr[i] << " ";
	std::cout << std::endl;
}

int digit_at(int number, int at)
{
	int n = 10;
	for (int i = 0; i < at - 1; i++)
		n *= 10;
	
	return (number % n) / (n / 10);
}

int get_digit_count(int number)
{
	int dig = 0;
	while (number != NULL)
	{
		number /= 10;
		++dig;
	}

	return dig;
}

int get_max(int* arr, int size)
{
	int max = arr[0];

	for (int i = 1; i < size; i++)
	{
		if (arr[i] > arr[i - 1])
			max = arr[i];
	}

	return max;
}

void set_array_by_lists(int* arr)
{
	int add_index = 0;
	for (int i = 0; i < sizeof(s) / sizeof(s[0]); i++)
	{
		for (int j = 0; j < s[i].length; j++)
			arr[add_index++] = s[i].get_element_at(j);
	}
}

void calculation(int* arr, int size)
{
	int max = get_max(arr, size);
	int digit_count = get_digit_count(max);
	
	for (int i = 0; i < size; i++)
	{
		int digit = digit_at(arr[i], digit_count);
		s[digit].add(arr[i]);
	}

	set_array_by_lists(arr);
}

int main()
{
	int size, *arr;

	std::cout << "Enter size: ";
	std::cin >> size;

	arr = new int[size];
	int value;
	for (int i = 0; i < size; i++)
	{
		std::cout << "Enter " << i + 1 << " element: ";
		std::cin >> value;
		arr[i] = value;
	}

	calculation(arr, size);

	print(arr, size);

	return 0;
}