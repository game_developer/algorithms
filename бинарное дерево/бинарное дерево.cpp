#include "stdafx.h"

struct node
{
	int value;
	node *right, *left;
	node(int num)
	{
		this->value = num;
		right = nullptr;
		left = nullptr;
	}
};

class tree
{
	node* head;

	node* get_min(node* n)
	{
		if (n == nullptr)
			std::cout << "ОШИБКА! Метод get_right работает криво! или же туда нулл впихнули..." << std::endl;

		if (n->left != nullptr)
			return get_min(n->left);

		return n;
	}

	node* get_father(node* helper, node* child)
	{
		if (helper == nullptr || helper->right == child || helper->left == child)
			return helper;

		node* right = get_father(helper->right, child);
		return right != nullptr ? right : get_father(helper->right, child);
	}

	bool add(node*& n, int num)
	{
		if (n == nullptr)
		{
			n = new node(num);
			return true;
		}

		if (n->value == num)
			return false;

		if (num > n->value)
			return add(n->right, num);

		return add(n->left, num);
	}

	bool remove(node*& n, int num)
	{
		if (n == NULL)
			return false;
		
		else if (num < n->value)
			return remove(n->left, num);
		
		else if (num > n->value)
			return remove(n->right, num);
		
		else if (n->left !=	NULL && n->right != NULL)
			remove_two(n);
		
		else
		{
			node* left = n->left;
			node* right = n->right;
			
			delete n;
			
			if (left == NULL)
				n = right;
			
			else if (right == NULL)
				n = left;
		}
		return n;
	}

	void remove_two(node* n)
	{
		node* min = get_min(n->right);
		n->value = min->value;

		remove(n->right, n->value);
	}

	void preorder(node* n)
	{
		if (n == nullptr)
			return;

		std::cout << n->value << std::endl;
		preorder(n->left);
		preorder(n->right);
	}

	void inorder(node* n)
	{
		if (n == nullptr)
			return;

		inorder(n->left);
		std::cout << n->value << " ";
		inorder(n->right);
	}

	void postorder(node* n)
	{
		if (n == nullptr)
			return;

		postorder(n->left);
		postorder(n->right);
		std::cout << n->value << std::endl;
	}

public:
	tree()
	{
		head = nullptr;
	}

	~tree()
	{
		free_memory(head);
	}

	void free_memory(node*& n)
	{
		if (n != nullptr)
		{
			free_memory(n->right);
			free_memory(n->left);

			delete n;
		}
	}

	bool add(int num)
	{
		return add(head, num);
	}

	bool remove(int n)
	{
		return remove(head, n);
	}

	void preorder()
	{
		preorder(head);
	}

	void inorder()
	{
		inorder(head);
	}

	void postorder()
	{
		postorder(head);
	}
};

int main()
{
	tree t;

	t.add(500);
	t.add(400);
	t.add(250);
	t.add(300);
	t.add(600);
	t.add(800);
	t.add(650);
	t.add(550);
	t.add(450);
	t.add(200);
	t.add(220);
	t.add(280);
	t.add(900);

	t.inorder();

	std::cout << "\n";

	t.remove(450);

	t.inorder();

	std::cout << "\n";

	t.remove(500);

	t.inorder();

	std::cout << "\n";

	t.remove(550);

	t.inorder();

	std::cout << "\n";

	return 0;
}
