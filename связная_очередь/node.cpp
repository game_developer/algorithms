#pragma once
#include "stdafx.h"

template <typename T>
struct node
{	
	T data;
	node* next;

	node(T data)
	{
		this->data = data;
		next = nullptr;
	}
};
