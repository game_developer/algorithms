#include "stdafx.h"
#include "node.cpp"
#include <iostream>

template<typename T>
class linked_stack
{
	node<T>* head_;
public:
	linked_stack()
	{
		head_ = nullptr;
	}
	
	bool push(T item)
	{
		node<T>* add = new node<T>(item);
		node<T>* helper = head_;

		head_ = add;
		head_->next = helper;
	
	
		return true;
	}
	
	T pop()
	{
		if (head_ == nullptr)
			return NULL;

		T ret = head_->data;
		node<T>* next = head_->next;

		delete head_;

		head_ = next;

		return ret;
	}
	
	void print()
	{
		if (head_ == nullptr)
			std::cout << "EMPTY";

		node<T>* helper = head_;

		while (helper != nullptr)
		{
			std::cout << helper->data << " ";

			helper = helper->next;
		}
		std::cout << std::endl;
	}
};