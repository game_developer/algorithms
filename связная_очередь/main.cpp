#include "stdafx.h"
#include "linked_list.cpp"
#include "linked_stack.cpp"
#include "linked_queue.cpp"

int main()
{
	linked_list<int> l;

	l.add_at(0, 9);

	l.remove_at(0);

	l.print();

	return 0;
}