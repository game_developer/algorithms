#include "stdafx.h"
#include "node.cpp"
#include <iostream>

template<typename T>
class linked_list
{
private:
	node<T>* head_;
	int index_of_last_;
	
	node<T>* get_node_at(int index)
	{
		node<T>* helper = head_;
		int i = 0;
		
		while (i != index)
		{
			i++;
			helper = helper->next;
		}

		return helper;
	}
public:
	linked_list()
	{
		head_ = nullptr;
		index_of_last_ = -1;
	}

	bool add_at(int index, T data)
	{
		if (index < 0 || index > index_of_last_ + 1)
			return false;
		
		node<T>* add = new node<T>(data);

		if (index == 0)
		{
			node<T>* helper = head_;
			head_ = add;
			head_->next = helper;
		}

		else
		{
			node<T>* prev = get_node_at(index - 1);
			node<T>* n = prev->next;
			
			prev->next = add;
			prev->next->next = n;
		}
		
		++index_of_last_;
		return true;
	}

	T remove_at(int index)
	{
		if (index < 0 || index > index_of_last_)
			return NULL;

		T ret;

		if (index == 0)
		{
			node<T>* helper = head_;
			head_ = head_->next;

			ret = helper->data;

			delete helper;
		}

		else
		{
			node<T>* prev = get_node_at(index - 1);
			node<T>* del = prev->next;

			ret = del->data;

			prev->next = prev->next->next;

			delete del;
		}

		--index_of_last_;
		return ret;
	}

	void print()
	{
		if (head_ == nullptr)
			std::cout << "EMPTY";

		node<T>* h = head_;

		while (h != nullptr)
		{
			std::cout << h->data << " ";
			h = h->next;
		}

		std::cout << "\n";
	}
};