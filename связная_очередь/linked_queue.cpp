#include "stdafx.h"
#include "node.cpp"
#include <iostream>

template<typename T>
class linked_queue
{
	node<T>* head_;
	node<T>* last_;
public:
	linked_queue()
	{
		head_ = nullptr;
		last_ = nullptr;
	}
	
	bool enqueue(T data)
	{
		node<T>* to_enqueue = new node<T>(data);

		bool is_empty = head_ == nullptr;
		
		if (!is_empty)
			last_->next = to_enqueue;
		
		last_ = to_enqueue;
		
		if(is_empty)
			head_ = last_;
	
		return true;
	}
	
	T dequeue()
	{
		if (head_ == nullptr)
			return NULL;

		T ret = head_->data;
		node<T>* n = head_->next;

		delete head_;
		
		head_ = n;
		return ret;
	}
	
	void print()
	{
		if (head_ == nullptr)
			std::cout << "EMPTY";

		node<T>* helper = head_;

		while (helper != nullptr)
		{
			std::cout << helper->data << " ";

			helper = helper->next;
		}

		std::cout << std::endl;
	}
};
