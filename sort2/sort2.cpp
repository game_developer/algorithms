#include "stdafx.h"

void swap(int arr[], int i, int j)
{
	int temp = arr[i];
	arr[i] = arr[j];
	arr[j] = temp;
}

//делает так что пирамидка становится максимальной
//вверху будет наибольший елемент
void bolshe_vverh(int arr[], int size, int i)
{
	//пока приравняем его к ите, потом посмотрим если больше.
	int index_of_largest = i;
	int index_of_left = 2 * i + 1;
	int index_of_right = 2 * i + 2;

	// если левый больше корня
	if (index_of_left < size && arr[index_of_left] > arr[index_of_largest])
		index_of_largest = index_of_left;

	// если правый больше самого большого
	if (index_of_right < size && arr[index_of_right] > arr[index_of_largest])
		index_of_largest = index_of_right;

	// если самый большой не корень
	if (index_of_largest != i)
	{
		swap(arr, i, index_of_largest);

		// рекурсивно идем дальше
		bolshe_vverh(arr, size, index_of_largest);
	}
}

void piramida(int arr[], int size)
{
	// делаем так чтоб пирамидка наша была отсортирована.
	for (int i = size / 2 - 1; i >= 0; i--)
		bolshe_vverh(arr, size, i);

	// один по одному убираем елемент из пирамидки
	for (int i = size - 1; i >= 0; i--)
	{
		// ставим корень в конец, а ту штуку в начало.
		swap(arr, 0, i);

		// опять сортируем нашу пирамидку зная что на 0 индексе все плохо.
		bolshe_vverh(arr, i, 0);
	}
}

int get_max(int arr[], int size)
{
	int mx = arr[0];
	for (int i = 1; i < size; i++)
		if (arr[i] > mx)
			mx = arr[i];
	return mx;
}

void count(int arr[], int size, int exp)
{
	int* res = new int[size]; // результат
	int i, count[10] = { 0 };

	// записываем в каунт.
	for (i = 0; i < size; i++)
		count[(arr[i] / exp) % 10]++;

	// суммируем
	for (i = 1; i < 10; i++)
		count[i] += count[i - 1];

	// строим результат
	for (i = size - 1; i >= 0; i--)
	{
		res[count[(arr[i] / exp) % 10] - 1] = arr[i];
		count[(arr[i] / exp) % 10]--;
	}

	// чисто сеттим с реза в арр
	for (i = 0; i < size; i++)
		arr[i] = res[i];
}

void radix(int arr[], int size)
{
	// находим максимальное
	int m = get_max(arr, size);

	for (int exp = 1; m / exp > 0; exp *= 10)
		count(arr, size, exp);
}

int partition(int arr[], int low, int high)
{
	int pivot = arr[high];
	// индекс меньшего элемента
	int i = low - 1;

	for (int j = low; j <= high - 1; j++)
	{
		// если меньше пивота
		if (arr[j] <= pivot)
			swap(arr, ++i, j);
	}

	swap(arr, i + 1, high);

	return (i + 1);
}

void quick(int arr[], int low, int high)
{
	if (low < high)
	{
		int pi = partition(arr, low, high);

		// сортирует до и после партишина.
		quick(arr, low, pi - 1);
		quick(arr, pi + 1, high);
	}
}

void print(int arr[], int size)
{
	for (int i = 0; i < size; i++)
		printf("%d ", arr[i]);
	printf("\n");
}

int main()
{
	int arr[] = { 5, 10, 3, 11, 2, 4 };
	int size = sizeof(arr) / sizeof(arr[0]);

	piramida(arr, size);

	return 0;
}

